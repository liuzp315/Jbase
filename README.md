#Jbase，JavaEE管理系统基础框架
当前版本:0.0.1<br/>
QQ交流群号1：492587379<br/>
<br/>

#界面展示
* 后台管理界面<br/>
![IMG_0874](./docs/images/zz001.png)
![IMG_0874](./docs/images/zz002.png)
![IMG_0874](./docs/images/zz003.png)
![IMG_0874](./docs/images/zz004.png)
* CMS演示界面<br/>
![IMG_0874](./docs/images/zz005.png)
![IMG_0874](./docs/images/zz006.png)
<br/><br/>

#特点
* 免费完整开源：基于Apache License Version 2.0协议，源代码完全开源，无商业限制；<br/>
* 模块化开发与管理：基于Maven项目管理，后台管理、CMS、商城等业务功能模块分别构建与管理，方便部署与扩展；<br/>

#面向对象
* 基于SpringFramework构建的基础系统框架，为JavaEE项目提供参考；
* 开发人员可以下载源代码来进行学习交流；
* 开发者和团队也可以使用Jbase做为基础构建自己的业务系统；

#技术框架
* 核心框架：Spring Framework 4
* 安全框架：Apache Shiro 1.2
* 视图框架：Spring MVC 4
* 持久层框架：BeetlSql
* 数据库连接池：Alibaba Druid 1.0
* 日志管理：SLF4J 1.7、Logback
* JS框架：jQuery 1.10
* CSS框架：H-ui
* 富文本：Ueditor

#开发环境
建议开发者使用以下环境，这样避免版本带来的问题
* IDE:eclipse
* DB:Mysql5.5
* JDK:JAVA 7、J2EE6

#运行环境
* WEB服务器：Weblogic、Tomcat、WebSphere、JBoss、Jetty 等
* 数据库服务器：Mysql5.5
* 操作系统：Windows、Linux、Unix 等

#快速体验
* 将Jbase项目源码导入eclipse；
* 创建数据库jbase,注意：数据库使用utf-8编码，导入docs/database文件夹中的数据库脚本；
* 修改src\main\resources\db.properties文件中的数据库设置参数；
* 修改src\main\resources\config.properties文件：
<br/>  resource.uploadpath=上传图片的物理路径
<br/>  resource.image_url=上传图片的访问路径 
<br/>  resource.cdn_url=公共的脚本和静态资源文件（如Jquery等第三方库）
<br/>  cms.tplpath=模板文件的物理路径
* 管理员账号，用户名：admin 密码：111111
