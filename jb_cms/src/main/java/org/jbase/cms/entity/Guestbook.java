/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.entity.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: Column.java <br/>
 * Class: com.yxt.cms.entity.Column <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:05:59 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "cms_guestbook")
public class Guestbook extends BaseEntity {

	private static final long serialVersionUID = -5670061931019936373L;

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the workunit
	 */
	public String getWorkunit() {
		return workunit;
	}

	/**
	 * @param workunit
	 *            the workunit to set
	 */
	public void setWorkunit(String workunit) {
		this.workunit = workunit;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the reUserId
	 */
	public int getReUserId() {
		return reUserId;
	}

	/**
	 * @param reUserId
	 *            the reUserId to set
	 */
	public void setReUserId(int reUserId) {
		this.reUserId = reUserId;
	}

	/**
	 * @return the reDate
	 */
	public Date getReDate() {
		return reDate;
	}

	/**
	 * @param reDate
	 *            the reDate to set
	 */
	public void setReDate(Date reDate) {
		this.reDate = reDate;
	}

	/**
	 * @return the reContent
	 */
	public String getReContent() {
		return reContent;
	}

	/**
	 * @param reContent
	 *            the reContent to set
	 */
	public void setReContent(String reContent) {
		this.reContent = reContent;
	}

	private int type = 0;
	private String title;
	private String content;
	private String name;
	private String email;
	private String phone;
	private String workunit;
	private String ip;
	private int reUserId;
	private Date reDate;
	private String reContent;
}
