/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.entity;

import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.entity.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: Column.java <br/>
 * Class: com.yxt.cms.entity.Column <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:05:59 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "cms_comment")
public class Comment extends BaseEntity {

	private static final long serialVersionUID = -7192991577976326500L;

	/**
	 * @return the columnId
	 */
	public int getColumnId() {
		return columnId;
	}

	/**
	 * @param columnId
	 *            the columnId to set
	 */
	public void setColumnId(int columnId) {
		this.columnId = columnId;
	}

	/**
	 * @return the columnSid
	 */
	public String getColumnSid() {
		return columnSid;
	}

	/**
	 * @param columnSid
	 *            the columnSid to set
	 */
	public void setColumnSid(String columnSid) {
		this.columnSid = columnSid;
	}

	/**
	 * @return the articleId
	 */
	public int getArticleId() {
		return articleId;
	}

	/**
	 * @param articleId
	 *            the articleId to set
	 */
	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	/**
	 * @return the articleSid
	 */
	public String getArticleSid() {
		return articleSid;
	}

	/**
	 * @param articleSid
	 *            the articleSid to set
	 */
	public void setArticleSid(String articleSid) {
		this.articleSid = articleSid;
	}

	/**
	 * @return the articleTitle
	 */
	public String getArticleTitle() {
		return articleTitle;
	}

	/**
	 * @param articleTitle
	 *            the articleTitle to set
	 */
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	/**
	 * @return the articleUrl
	 */
	public String getArticleUrl() {
		return articleUrl;
	}

	/**
	 * @param articleUrl
	 *            the articleUrl to set
	 */
	public void setArticleUrl(String articleUrl) {
		this.articleUrl = articleUrl;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the userSid
	 */
	public String getUserSid() {
		return userSid;
	}

	/**
	 * @param userSid
	 *            the userSid to set
	 */
	public void setUserSid(String userSid) {
		this.userSid = userSid;
	}

	/**
	 * @return the toUserId
	 */
	public int getToUserId() {
		return toUserId;
	}

	/**
	 * @param toUserId
	 *            the toUserId to set
	 */
	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}

	/**
	 * @return the toUserSid
	 */
	public String getToUserSid() {
		return toUserSid;
	}

	/**
	 * @param toUserSid
	 *            the toUserSid to set
	 */
	public void setToUserSid(String toUserSid) {
		this.toUserSid = toUserSid;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the auditUserId
	 */
	public int getAuditUserId() {
		return auditUserId;
	}

	/**
	 * @param auditUserId
	 *            the auditUserId to set
	 */
	public void setAuditUserId(int auditUserId) {
		this.auditUserId = auditUserId;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate
	 *            the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the parentid
	 */
	public int getParentid() {
		return parentid;
	}

	/**
	 * @param parentid
	 *            the parentid to set
	 */
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}

	private int columnId;
	private String columnSid;
	private int articleId;
	private String articleSid;
	private String articleTitle;
	private String articleUrl;
	private String content;
	private int userId;
	private String userSid;
	private int toUserId;
	private String toUserSid;
	private String userName;
	private String userEmail;
	private String ip;
	private int auditUserId;
	private Date auditDate;
	private int type;
	private int parentid;
}
