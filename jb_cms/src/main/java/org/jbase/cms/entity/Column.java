/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.entity;

import org.beetl.sql.core.annotatoin.Table;
import org.jbase.common.entity.BaseEntity;

/**
 * Project: fw_common <br/>
 * File: Column.java <br/>
 * Class: com.yxt.cms.entity.Column <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:05:59 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "cms_columns")
public class Column extends BaseEntity {

	public static final int SHOW_MODE_COLUMN = 0;
	public static final int SHOW_MODE_PAGE = 1;
	public static final int SHOW_MODE_URL = 2;

	private static final long serialVersionUID = 3978544762788129008L;

	/**
	 * @return the parentId
	 */
	public int getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the parentIds
	 */
	public String getParentIds() {
		return parentIds;
	}

	/**
	 * @param parentIds
	 *            the parentIds to set
	 */
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId
	 *            the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the officeId
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * @param officeId
	 *            the officeId to set
	 */
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	/**
	 * @return the module
	 */
	public String getModule() {
		return module;
	}

	/**
	 * @param module
	 *            the module to set
	 */
	public void setModule(String module) {
		this.module = module;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords
	 *            the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the sort
	 */
	public int getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            the sort to set
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}

	/**
	 * @return the inMenu
	 */
	public int getInMenu() {
		return inMenu;
	}

	/**
	 * @param inMenu
	 *            the inMenu to set
	 */
	public void setInMenu(int inMenu) {
		this.inMenu = inMenu;
	}

	/**
	 * @return the inList
	 */
	public int getInList() {
		return inList;
	}

	/**
	 * @param inList
	 *            the inList to set
	 */
	public void setInList(int inList) {
		this.inList = inList;
	}

	/**
	 * @return the showModes
	 */
	public int getShowModes() {
		return showModes;
	}

	/**
	 * @param showModes
	 *            the showModes to set
	 */
	public void setShowModes(int showModes) {
		this.showModes = showModes;
	}

	/**
	 * @return the allowComment
	 */
	public int getAllowComment() {
		return allowComment;
	}

	/**
	 * @param allowComment
	 *            the allowComment to set
	 */
	public void setAllowComment(int allowComment) {
		this.allowComment = allowComment;
	}

	/**
	 * @return the isAudit
	 */
	public int getIsAudit() {
		return isAudit;
	}

	/**
	 * @param isAudit
	 *            the isAudit to set
	 */
	public void setIsAudit(int isAudit) {
		this.isAudit = isAudit;
	}

	/**
	 * @return the customListView
	 */
	public String getCustomListView() {
		return customListView;
	}

	/**
	 * @param customListView
	 *            the customListView to set
	 */
	public void setCustomListView(String customListView) {
		this.customListView = customListView;
	}

	/**
	 * @return the customContentView
	 */
	public String getCustomContentView() {
		return customContentView;
	}

	/**
	 * @param customContentView
	 *            the customContentView to set
	 */
	public void setCustomContentView(String customContentView) {
		this.customContentView = customContentView;
	}

	/**
	 * @return the viewConfig
	 */
	public String getViewConfig() {
		return viewConfig;
	}

	/**
	 * @param viewConfig
	 *            the viewConfig to set
	 */
	public void setViewConfig(String viewConfig) {
		this.viewConfig = viewConfig;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the showChild
	 */
	public int getShowChild() {
		return showChild;
	}

	/**
	 * @param showChild
	 *            the showChild to set
	 */
	public void setShowChild(int showChild) {
		this.showChild = showChild;
	}

	private int parentId;
	private String parentIds;
	private String siteId;
	private String officeId;
	private String module;
	private String name;
	private String image;
	private String href;
	private String target;
	private String description;
	private String keywords;
	private int sort = 0;
	private int inMenu = 1;
	private int inList = 1;
	private int showModes = 0;
	private int allowComment = 1;
	private int isAudit = 0;
	private String customListView = "/tpl/column";
	private String customContentView = "/tpl/article";
	private String viewConfig;
	private int level = 0;
	private int showChild = 0;
}
