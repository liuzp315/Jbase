/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.GlobalConstants;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: TplService.java <br/>
 * Class: com.yxt.cms.service.TplService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月21日 上午11:10:55 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class TplService {

	public List<Map<String, String>> listTplNames() {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		String tmpFilePath = GlobalConstants.cms_template_path;
		File tmpFileDir = new File(tmpFilePath);
		if (!(tmpFileDir.exists() && tmpFileDir.isDirectory()))
			return list;
		String[] list2 = tmpFileDir.list();
		for (String string : list2) {
			if (string.endsWith(".html")) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", string);
				map.put("value", string.substring(0, string.indexOf(".html")));
				list.add(map);
			}
		}
		return list;
	}
}
