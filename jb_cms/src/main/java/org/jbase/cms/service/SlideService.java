/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.cms.entity.Slide;
import org.jbase.cms.entity.SlideCat;
import org.jbase.cms.ext.beetl.CmsFunctionPackage;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: ColumnService.java <br/>
 * Class: com.yxt.cms.service.ColumnService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:14:01 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class SlideService extends EntityService<Slide> {

	@Autowired
	private CmsFunctionPackage cmsFunctionPackage;

	@Autowired
	private SlideCatService slideCatService;

	@Override
	protected Class<Slide> clzz() {
		return Slide.class;
	}

	@Override
	protected Map<String, Object> listWithPageWhere(Map<String, String> map) {

		StringBuilder sb = new StringBuilder(" 1=1 ");
		for (String key : map.keySet()) {
			if ("cid".equals(key)) {
				sb.append(" and cid=" + map.get(key));
			} else {
				sb.append(" and " + key + " like '%" + map.get(key) + "%'");
			}
		}
		String tableName = sqlManager.getNc().getTableName(clzz());
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("table_name", tableName);
		where.put("sql_where", sb.toString());
		return where;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param string
	 * @return
	 */
	public List<Slide> listByCatId(String catId) {
		Map<String, Object> paras = new HashMap<String, Object>();
		paras.put("catId", catId);
		return sqlManager.select("cms_slide.listByCatId", Slide.class, paras);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param catId
	 * @return
	 */
	public SlideCat fetchSlideCat(int catId) {
		SlideCat cat = slideCatService.fetchById(catId);
		cat.set("slideList", listByCatId("" + catId));
		return cat;
	}

	@Override
	public int insertHolderId(Map<String, Object> map) {
		int re = super.insertHolderId(map);
		Object cid = map.get("cid");
		cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		return re;
	}

	@Override
	public int update(Map<String, Object> map) {
		int re = super.update(map);
		Object cid = map.get("cid");
		cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		return re;
	}

	@Override
	public int deleteById(int id) {
		Slide s = fetchById(id);
		Object cid = s.getCid();
		cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		int re = super.deleteById(id);
		return re;
	}

	@Override
	public void deleteByIds(String ids) {
		String[] split = ids.split(",");
		for (String id : split) {
			int cid = fetchById(Integer.parseInt(id)).getCid();
			cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		}

		super.deleteByIds(ids);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param ids
	 * @param sorts
	 * @return
	 */
	public boolean updateSort(String[] ids, String[] sorts) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		int length = ids.length;
		for (int i = 0; i < length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", ids[i]);
			map.put("sort", sorts[i]);
			list.add(map);
			int cid = fetchById(Integer.parseInt(ids[i])).getCid();
			cmsFunctionPackage.remave(CmsFunctionPackage.SLIDE_CAT_WITH_CHILDS + cid);
		}
		int[] updateByIdBatch = sqlManager.updateBatch("cms_slide.updateSort", list);
		return updateByIdBatch.length == length ? true : false;
	}
}
