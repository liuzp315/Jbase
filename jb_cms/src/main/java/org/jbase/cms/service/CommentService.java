/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.cms.entity.Comment;
import org.jbase.common.service.EntityService;
import org.springframework.stereotype.Service;

/**
 * Project: fw_common <br/>
 * File: ColumnService.java <br/>
 * Class: com.yxt.cms.service.ColumnService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:14:01 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class CommentService extends EntityService<Comment> {

	@Override
	protected Class<Comment> clzz() {
		return Comment.class;
	}
	public List<Comment> findCommentListBycomment(int articleId) {
		List<Comment> list = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("articleId",articleId );
					
			list = sqlManager.select("cms_comment.serchAndContent", Comment.class, map);
		
		return list;
	}

	
	
	
}
