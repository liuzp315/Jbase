/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.service;

import java.util.List;
import java.util.Map;

import org.beetl.sql.core.db.KeyHolder;
import org.jbase.cms.entity.SiteConfig;
import org.jbase.cms.ext.beetl.CmsFunctionPackage;
import org.jbase.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: fw_cms <br/>
 * File: SiteConfigService.java <br/>
 * Class: com.yxt.cms.service.SiteConfigService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月23日 下午5:17:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class SiteConfigService extends BaseService {

	@Autowired
	private CmsFunctionPackage cmsFunctionPackage;

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @return
	 */
	public SiteConfig fetchInfo() {
		List<SiteConfig> all = sqlManager.all(SiteConfig.class);
		return all.isEmpty() ? null : all.get(0);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param params
	 * @return
	 */
	public SiteConfig insertOrUpdate(Map<String, Object> params) {
		SiteConfig sc = new SiteConfig();
		sc.setSiteTitle(params.get("siteTitle").toString());
		sc.setSiteName(params.get("siteName").toString());
		sc.setSiteCopy(params.get("siteCopy").toString());
		sc.setSiteIcp(params.get("siteIcp").toString());
		sc.setLogoUrl(params.get("logoUrl").toString());
		sc.setKeywords(params.get("keywords").toString());
		sc.setDescription(params.get("description").toString());
		sc.setSummary(params.get("summary").toString());
		String id = params.get("id").toString();
		if (id == null || id.isEmpty()) {
			KeyHolder keyHolder = new KeyHolder();
			sqlManager.insert(SiteConfig.class, sc, keyHolder);
			sc.setId(keyHolder.getInt());
		} else {
			updateById(SiteConfig.class, params);
			sc.setId(Integer.parseInt(id));
		}
		cmsFunctionPackage.remave(CmsFunctionPackage.SITE_CONFIG);
		return sc;
	}

}
