package org.jbase.cms.controller;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Guestbook;
import org.jbase.cms.service.GuestbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("cms_guestbook_controller")
@RequestMapping("/guestbook")
public class GuestBookController {
	@Autowired
	private GuestbookService guestbookService;

	@RequestMapping("/guestbookDo")
	public String guestbookDo(HttpServletRequest request) {
		Guestbook gu = new Guestbook();
		gu.setName(request.getParameter("name"));
		gu.setEmail(request.getParameter("email"));
		gu.setContent(request.getParameter("content"));
		guestbookService.insert(gu);
		return "redirect:/index";
	}

}
