/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Article;
import org.jbase.cms.entity.Column;
import org.jbase.cms.service.ArticleService;
import org.jbase.cms.service.ColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Project: fw_cms <br/>
 * File: ColumnController.java <br/>
 * Class: com.yxt.cms.controller.ColumnController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月9日 下午4:55:08 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("cms_column_controller")
@RequestMapping("/column")
public class ColumnController {

	@Autowired
	private ColumnService columnService;

	@Autowired
	private ArticleService articleService;

	@RequestMapping("/{columnId}/{childId}")
	public String index(@PathVariable Integer columnId, @PathVariable Integer childId, HttpServletRequest request) {
		Column column = columnService.fetchById(columnId);
		List<Column> childList = columnService.findChildListByParentId(columnId);
		List<Article> articleList = null;
		if (columnId == childId) {
			articleList = articleService.listByColumn(column);
		} else {
			articleList = articleService.listByColumnId(childId);
		}
		request.setAttribute("columnId", columnId);
		request.setAttribute("childId", childId);
		request.setAttribute("column", column);
		request.setAttribute("list", childList);
		request.setAttribute("articleList", articleList);

		return column.getCustomListView();

	}

	@RequestMapping("/{columnId}")
	public String showcolumn(@PathVariable Integer columnId, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		Column column = columnService.fetchById(columnId);
		int showMode = column.getShowModes();
		String view = column.getCustomListView();
		if (Column.SHOW_MODE_PAGE == showMode) {
			view = column.getCustomContentView();
		}
		if (Column.SHOW_MODE_URL == showMode) {
			view = "redirect:" + column.getHref();
		}
		int childColumnId = columnId;
		if (column.getParentId() != 0) {
			columnId = column.getParentId();
		}
		redirectAttributes.addFlashAttribute("column_id", columnId);
		redirectAttributes.addFlashAttribute("child_column_id", childColumnId);
		redirectAttributes.addFlashAttribute("column", column);
		request.setAttribute("column_id", columnId);
		request.setAttribute("child_column_id", childColumnId);
		request.setAttribute("column", column);

		return view;

	}

}
