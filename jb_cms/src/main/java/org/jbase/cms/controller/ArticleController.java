/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jbase.cms.entity.Article;
import org.jbase.cms.entity.Column;
import org.jbase.cms.service.ArticleService;
import org.jbase.cms.service.ColumnService;
import org.jbase.common.utils.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Project: fw_cms <br/>
 * File: ColumnController.java <br/>
 * Class: com.yxt.cms.controller.ColumnController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月9日 下午4:55:08 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("cms_article_controller")
@RequestMapping("/article")
public class ArticleController {

	@Autowired
	private ColumnService columnService;
	@Autowired
	private ArticleService articleService;

	@RequestMapping("/{id}")
	public String item(@PathVariable Integer id, HttpServletRequest request, RedirectAttributes redirectAttributes) {

		Article article = articleService.fetchById(id);
		request.setAttribute("article", article);

		int childColumnId = article.getColumnId();
		int columnId = childColumnId;
		Column column = columnService.fetchById(childColumnId);
		if (column.getParentId() != 0) {
			columnId = column.getParentId();
		}

		String view = column.getCustomContentView();
		int showMode = column.getShowModes();
		if (Column.SHOW_MODE_PAGE == showMode) {
			view = column.getCustomContentView();
		}
		if (Column.SHOW_MODE_URL == showMode) {
			view = "redirect:" + column.getHref();
		}

		redirectAttributes.addFlashAttribute("column_id", columnId);
		redirectAttributes.addFlashAttribute("child_column_id", childColumnId);
		redirectAttributes.addFlashAttribute("column", column);
		request.setAttribute("column_id", columnId);
		request.setAttribute("child_column_id", childColumnId);
		request.setAttribute("column", column);

		return view;
	}

	@RequestMapping("/page")
	public String page(HttpServletRequest request) {
		int colId = 1;
		int start = 1;
		int size = 10;
		String columnId = request.getParameter("columnId");
		if (StringUtils.isNotEmpty(columnId))
			colId = Integer.parseInt(columnId);
		String pageStart = request.getParameter("pageStart");
		if (StringUtils.isNotEmpty(pageStart))
			start = Integer.parseInt(pageStart);
		String pageSize = request.getParameter("pageSize");
		if (StringUtils.isNotEmpty(pageSize))
			size = Integer.parseInt(pageSize);
		Pager pager = articleService.pageByColumnId(colId, start, size);

		request.setAttribute("pager", pager);

		return "/tpl/article_page";
	}
}
