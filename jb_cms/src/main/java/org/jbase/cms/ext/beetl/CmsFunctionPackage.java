/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.cms.ext.beetl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.cms.entity.Article;
import org.jbase.cms.entity.Column;
import org.jbase.cms.entity.SiteConfig;
import org.jbase.cms.entity.Slide;
import org.jbase.cms.entity.SlideCat;
import org.jbase.cms.service.ArticleService;
import org.jbase.cms.service.ColumnService;
import org.jbase.cms.service.SiteConfigService;
import org.jbase.cms.service.SlideService;
import org.jbase.common.utils.Pager;
import org.jbase.ext.beetl.JbaseFunctionPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Project: fw_cms <br/>
 * File: CmsFunctionPackage.java <br/>
 * Class: com.yxt.cms.ext.beetl.CmsFunctionPackage <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年1月21日 下午2:08:53 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service("cmsfn")
public class CmsFunctionPackage implements JbaseFunctionPackage {

	public static final String COLUMN_WITH_CHILDS = "cms.function.columnWithChilds";
	public static final String SITE_CONFIG = "cms.function.siteConfig";
	public static final String SLIDE_CAT_WITH_CHILDS = "cms.function.slideCatWithChilds";

	// 用作缓存，保存页面上的常量
	private static Map<String, Object> cache = new HashMap<String, Object>();

	/**
	 * 
	 * 描述 : 清除所有缓存 <br/>
	 * <p>
	 *
	 */
	public void clearCache() {
		cache.clear();
	}

	/**
	 * 
	 * 描述 : 清除指定缓存 <br/>
	 * <p>
	 * 
	 * @param key
	 *            缓存标识
	 */
	public void remave(String key) {
		cache.remove(key);
	}

	/**
	 * 
	 * 描述 : 获取指定缓存 <br/>
	 * <p>
	 * 
	 * @param key
	 *            缓存标识
	 * @return 缓存内容
	 */
	public Object get(String key) {
		return cache.get(key);
	}

	/**
	 * 
	 * 描述 : 设置指定缓存 <br/>
	 * <p>
	 * 
	 * @param key
	 *            缓存标识
	 * @param value
	 *            缓存内容
	 */
	public void set(String key, Object value) {
		cache.put(key, value);
	}

	@Autowired
	private ColumnService columnService;

	@Autowired
	private ArticleService articleService;

	@Autowired
	private SlideService slideService;

	@Autowired
	private SiteConfigService siteConfigService;

	/**
	 * 
	 * 描述 : 获取站点的所有信息 <br/>
	 * <p>
	 * 
	 * @return 站点配置信息
	 */
	public SiteConfig siteConfig() {
		SiteConfig fetchInfo = (SiteConfig) get(SITE_CONFIG);
		if (fetchInfo == null) {
			fetchInfo = siteConfigService.fetchInfo();
			set(SITE_CONFIG, fetchInfo);
		}
		return fetchInfo;

	}

	/**
	 * 
	 * 描述 : 获取所有可显示在导航条的顶级菜单项及其子菜单 <br/>
	 * <p>
	 * 
	 * @return
	 */
	public List<Column> columnWithChilds() {
		@SuppressWarnings("unchecked")
		List<Column> list = (List<Column>) get(COLUMN_WITH_CHILDS);
		if (list == null) {
			list = columnService.listNavWithChilds();
			set(COLUMN_WITH_CHILDS, list);
		}
		return list;
	}

	/**
	 * 
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param catId
	 * @return
	 */
	public SlideCat slideCat(int catId) {
		String cache_key = SLIDE_CAT_WITH_CHILDS + catId;
		SlideCat cat = (SlideCat) get(cache_key);
		if (cat == null) {
			cat = slideService.fetchSlideCat(catId);
			set(cache_key, cat);
		}
		return cat;
	}

	public List<Slide> listSlidesByCatId(int catId) {
		@SuppressWarnings("unchecked")
		List<Slide> list = (List<Slide>) slideCat(catId).get("slideList");
		return list;
	}

	public List<Column> listSubCloumns(int parentId) {
		return columnService.findChildListByParentId(parentId);
	}

	public List<Article> listArticleByCloumnId(int columnId) {
		return articleService.listByColumnId(columnId);
	}

	public List<Article> listAllArticlesByColumnId(int columnId) {
		return articleService.listAllByColumnId(columnId);
	}

	public Column findColumnByArticleId(int articleId) {
		return columnService.fetchByArticleId(articleId);
	}

	public List<Column> findColumnsByChildColumnId(int childId) {
		List<Column> cols = columnService.fetchByChildId(null, childId);
		Collections.reverse(cols);
		return cols;
	}

	public Pager pageArticlesByColumnId(int columnId, int start, int size) {
		return articleService.pageByColumnId(columnId, start, size);
	}
	@SuppressWarnings("unchecked")
	public List<Article> listArticles(int columnId, int start, int size) {
		return (List<Article>) pageArticlesByColumnId(columnId, start, size).getList();
	}
}
