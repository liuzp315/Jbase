/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.cms.controller;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.SiteConfig;
import org.jbase.cms.service.SiteConfigService;
import org.jbase.common.utils.Requester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_cms <br/>
 * File: SiteConfigController.java <br/>
 * Class: com.yxt.cms.controller.SiteConfigController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月23日 下午5:19:10 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_cms_controller")
@RequestMapping("/admin/cms")
public class SiteConfigController {

	@Autowired
	private SiteConfigService siteConfigService;

	@RequestMapping("/siteconfig")
	public void siteconfig(HttpServletRequest request) {
		SiteConfig sc = siteConfigService.fetchInfo();
		request.setAttribute("obj", sc);
	}

	@ResponseBody
	@RequestMapping("/siteconfigSave")
	public String siteconfigSave(HttpServletRequest request) {
		SiteConfig sc = siteConfigService.insertOrUpdate(Requester.warp(request).getParams());
		sc.set("retCode", "200");
		return JSON.toJSONString(sc);
	}

}
