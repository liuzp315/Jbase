/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.cms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jbase.cms.entity.Column;
import org.jbase.cms.service.ColumnService;
import org.jbase.cms.service.TplService;
import org.jbase.common.controller.EntityController;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Project: fw_admin <br/>
 * File: ColumnController.java <br/>
 * Class: com.yxt.admin.controller.cms.ColumnController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 下午6:15:46 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_cms_column_controller")
@RequestMapping("/admin/cms/column")
public class ColumnController extends EntityController<Column> {
	@Autowired
	private ColumnService columnService;
	@Autowired
	private TplService tplService;

	@Override
	protected EntityService<Column> service() {
		return columnService;
	}

	@Override
	public void list(HttpServletRequest request) {
		request.setAttribute("list", columnService.findAllList());
	}

	@Override
	public void add(HttpServletRequest request) {
		int pId = 0;
		String parentId = request.getParameter("parentId");
		if (!StringUtils.isEmpty(parentId))
			pId = Integer.parseInt(parentId);
		List<Column> parentList = columnService.findParentList();
		request.setAttribute("parentList", parentList);
		request.setAttribute("pId", pId);
		request.setAttribute("tplList", tplService.listTplNames());

		super.add(request);
	}

	@Override
	public void edit(int id, HttpServletRequest request) {
		List<Column> parentList = columnService.findParentList();
		request.setAttribute("parentList", parentList);
		request.setAttribute("tplList", tplService.listTplNames());
		super.edit(id, request);
	}

	@RequestMapping("/addArticle")
	public void addArticle(HttpServletRequest request) {
		int colId = 0;
		String columnId = request.getParameter("columnId");
		if (!StringUtils.isEmpty(columnId))
			colId = Integer.parseInt(columnId);
		request.setAttribute("columnlist", columnService.findAllList());
		request.setAttribute("colId", colId);
		request.setAttribute("tplList", tplService.listTplNames());
	}

	@ResponseBody
	@RequestMapping(value = "/updateSort", produces = "text/plain;charset=UTF-8")
	public String updateSort(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String[] ids = request.getParameterValues("ids");
		String[] sorts = request.getParameterValues("sorts");
		if (ids.length != sorts.length)
			return fail("保存失败，保存的数据条数不一致！");
		boolean res = columnService.updateSort(ids, sorts);
		if (res)
			return ok();
		else
			return fail("保存失败！");
	}
}
