
searchAndPager
===
select ca.*,cc.name as colName from cms_article ca left join cms_columns cc on ca.columnId=cc.id where #text(sql_where)# order by columnId,ca.sort desc,ca.createTime desc

listAllByColumnId
===
select * from cms_article where columnId in (select id from cms_columns where id=#columnId# or parentId=#columnId#) order by columnId, sort desc,createTime desc

listByColumnId
===
select * from cms_article where columnId = #columnId# order by columnId, sort desc,createTime desc

pageByColumnId_count
===
select count(id) from cms_article where columnId in (select id from cms_columns where id=#columnId# or parentId=#columnId#)

pageByColumnId_page
===
select * from cms_article where columnId in (select id from cms_columns where id=#columnId# or parentId=#columnId#)  order by sort desc,createTime desc

updateSort
===
update cms_article set sort=#sort# where id = #id#