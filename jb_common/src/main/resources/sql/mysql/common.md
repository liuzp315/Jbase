
count
===
select count(id) from #text(table_name)# where #text(sql_where)#

searchAndPager
===
select * from #text(table_name)# where #text(sql_where)#

selectSingleBySid
===
select * from #text(table_name)# where sid = #sid#

deleteByIds
===
delete from #text(table_name)# where id in (#text(ids)#)

deleteBySid
===
delete from #text(table_name)# where sid = #sid#

fetchById
===
select * from #text(table_name)# where id=#id#

fetchBySId
===
select * from #text(table_name)# where sid = #sid#