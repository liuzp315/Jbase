/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.ext.beetl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.jbase.GlobalConstants;
import org.jbase.common.utils.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Project: fw_admin <br/>
 * File: AdminBeetlGroupUtilConfiguration.java <br/>
 * Class: com.yxt.admin.ext.beetl.AdminBeetlGroupUtilConfiguration <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年11月30日 下午2:46:53 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public class JbaseBeetlGroupUtilConfiguration extends BeetlGroupUtilConfiguration {

	@Autowired
	private Map<String, JbaseFunctionPackage> funcpack;

	private String ctxPath;
	private String rootPath;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.beetl.ext.spring.BeetlGroupUtilConfiguration#setServletContext(javax.servlet.ServletContext)
	 */
	@Override
	public void setServletContext(ServletContext sc) {
		super.setServletContext(sc);
		rootPath = sc.getRealPath("/");
		ctxPath = sc.getContextPath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.beetl.ext.spring.BeetlGroupUtilConfiguration#initOther()
	 */
	@Override
	protected void initOther() {
		super.initOther();
		initSharedVars();

		// 注册自定义函数包
		for (Entry<String, JbaseFunctionPackage> entry : funcpack.entrySet()) {
			groupTemplate.registerFunctionPackage(entry.getKey(), entry.getValue());
		}

		initGlobalConstants();

	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 */
	private void initSharedVars() {
		if (sharedVars == null)
			sharedVars = new HashMap<String, Object>();
		String _base = ctxPath;
		String hasNginx = ConfigProperties.getValue("has_nginx");
		if ("true".equals(hasNginx)) {
			_base = "";
		}
		sharedVars.put("BASE", _base);
		sharedVars.put("STATIC", _base + "/static");
		String imageurl = ConfigProperties.getValue("resource.image_url");
		if (StringUtils.isEmpty(imageurl)) {
			imageurl = _base + "/upload";
		}
		sharedVars.put("IMAGEPATH", imageurl);
		groupTemplate.setSharedVars(sharedVars);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 */
	private void initGlobalConstants() {
		String uploadpath = ConfigProperties.getValue("resource.uploadpath");
		if (StringUtils.isEmpty(uploadpath)) {
			uploadpath = rootPath + "/upload";
		}
		GlobalConstants.image_upload_path = uploadpath;
		
		String cmstplpath = ConfigProperties.getValue("cms.tplpath");
		if (StringUtils.isEmpty(cmstplpath)) {
			cmstplpath = rootPath + "/WEB-INF/classes/template/tpl";
		}
		GlobalConstants.cms_template_path = cmstplpath;
	}

}
