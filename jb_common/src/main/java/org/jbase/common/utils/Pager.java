/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

/**
 * Project: fw_common <br/>
 * File: Pager.java <br/>
 * Class: com.yxt.common.utils.Pager <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月17日 上午9:57:17 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public class Pager implements Serializable {

	private static final long serialVersionUID = 8292402990878486465L;

	/**
	 * 改变这个，当每页大小超过 MAX_FETCH_SIZE 时，这个将是默认的 fetchSize
	 */
	public static int DEFAULT_PAGE_SIZE = 10;

	/**
	 * ResultSet 最大的 fetch size
	 */
	public static int MAX_FETCH_SIZE = 200;

	// private static final int FIRST_PAGE_NUMBER = 1;

	private int pageNumber;
	private int pageSize;
	private int pageCount;
	private int recordCount;

	private List<?> list;

	public List<?> getList() {
		if (list == null)
			list = new ArrayList<Object>();
		return list;
	}

	public Pager setList(List<?> list) {
		this.list = list;
		return this;
	}

	public Pager() {
		pageNumber = 1;
		pageSize = DEFAULT_PAGE_SIZE;
	}

	public Pager(int pageNumber) {
		this.pageNumber = pageNumber;
		pageSize = DEFAULT_PAGE_SIZE;
	}

	public Pager(int pageNumber, int pageSize) {
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	public Pager(String pageNumber) {
		if (StringUtils.isEmpty(pageNumber))
			this.pageNumber = 1;
		else
			this.pageNumber = Integer.parseInt(pageNumber);
		pageSize = DEFAULT_PAGE_SIZE;
	}

	public Pager(String pageNumber, String pageSize) {
		if (StringUtils.isEmpty(pageNumber))
			this.pageNumber = 1;
		else
			this.pageNumber = Integer.parseInt(pageNumber);
		if (StringUtils.isEmpty(pageSize))
			this.pageSize = DEFAULT_PAGE_SIZE;
		else
			this.pageSize = Integer.parseInt(pageSize);
	}

	public Pager resetPageCount() {
		pageCount = -1;
		return this;
	}

	public int getPageCount() {
		if (pageCount < 0)
			pageCount = (int) Math.ceil((double) recordCount / pageSize);
		return pageCount;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public Pager setPageNumber(int pn) {
		pageNumber = pn;
		return this;
	}

	public Pager setPageSize(int pageSize) {
		this.pageSize = (pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE);
		return resetPageCount();
	}

	public Pager setRecordCount(int recordCount) {
		this.recordCount = recordCount > 0 ? recordCount : 0;
		this.pageCount = (int) Math.ceil((double) recordCount / pageSize);
		return this;
	}

	public int getOffset() {
		return (pageSize * (pageNumber - 1)) + 1;
	}

	@Override
	public String toString() {
		return String.format("size: %d, total: %d, page: %d/%d", pageSize, recordCount, pageNumber,
				this.getPageCount());
	}

	public boolean isFirst() {
		return pageNumber == 1;
	}

	public boolean isLast() {
		if (pageCount == 0)
			return true;
		return pageNumber == pageCount;
	}

	private int draw = 1;
	private int start = 0;
	private boolean isDataTables = false;

	public void setIsDataTables(boolean ist) {
		this.isDataTables = ist;
	}

	public int getStart() {
		if (isDataTables)
			return start + 1;
		else
			return getOffset();
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getDraw() {
		return draw;
	}

	public int getRecordsTotal() {
		return recordCount;
	}

	public int getRecordsFiltered() {
		return recordCount;
	}

	public List<?> getData() {
		return list;
	}
}
