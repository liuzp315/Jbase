/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * Project: fw_common <br/>
 * File: WebUtils.java <br/>
 * Class: com.yxt.common.utils.WebUtils <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月15日 下午5:37:03 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public class Requester {

	private Pager pager = new Pager();;
	private Map<String, Object> params = new HashMap<String, Object>();
	private Map<String, String> searchKeys = new HashMap<String, String>();

	private boolean isSearch = false;

	public Map<String, Object> getParams() {
		return params;
	}

	public Pager getPager() {
		if (isSearch)
			pager.setPageNumber(1);
		return pager;
	}

	public Map<String, String> getSearchKeys() {
		return searchKeys;
	}

	public static Requester warp(HttpServletRequest request) {
		Requester req = new Requester();
		Enumeration<?> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = (String) parameterNames.nextElement();
			String parameter = request.getParameter(name);
			if (parameter.isEmpty())
				continue;
			parameter = parameter.trim();

			if ("_".equals(name)) {
			} else if ("_sbtn".equals(name) && "true".equals(parameter)) {
				req.isSearch = true;
			} else if ("draw".equals(name)) {
				req.pager.setDraw(Integer.parseInt(parameter));
			} else if ("start".equals(name)) {
				req.pager.setStart(Integer.parseInt(parameter));
				req.pager.setIsDataTables(true);
			} else if ("length".equals(name)) {
				req.pager.setPageSize(Integer.parseInt(parameter));
			} else if (name.startsWith("columns[")) {
			} else if (name.startsWith("search[")) {
			} else if (name.startsWith("_p_")) {
				req.pager.setIsDataTables(false);
				String p = name.substring(3);
				if ("pageNumber".equals(p))
					req.pager.setPageNumber(Integer.parseInt(parameter));
				if ("pageSize".equals(p))
					req.pager.setPageSize(Integer.parseInt(parameter));
			} else if (name.startsWith("_s_")) {
				String p = name.substring(3);
				req.searchKeys.put(p, parameter);
				request.setAttribute("searchKeys", req.searchKeys);
			} else {
				req.params.put(name, parameter);
			}
		}
		return req;
	}
}
