/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.service;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jbase.GlobalConstants;
import org.jbase.common.entity.Attachment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Project: fw_common <br/>
 * File: AttachmentService.java <br/>
 * Class: com.yxt.common.service.AttachmentService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月27日 上午12:26:06 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class AttachmentService extends BaseService {

	private static final DateFormat df = new SimpleDateFormat("yyyyMMdd");

	public Attachment createFile(MultipartFile file) {
		Attachment attach = new Attachment();

		String dt = df.format(new Date());
		String fileName = file.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
		String sname = System.currentTimeMillis() + (Math.random() + "").replace(".", "") + suffix;
		String lpath = GlobalConstants.image_upload_path + "/" + dt;
		File pfile = new File(lpath);
		if (!pfile.exists())
			pfile.mkdirs();
		attach.setMimeType(file.getContentType());
		attach.setSize(file.getSize());
		attach.setUrl(dt + "/" + sname);
		attach.setPath(lpath + "/" + sname);
		attach.setAftName(sname);
		attach.setPreName(fileName);
		attach.setSuffix(suffix);
		sqlManager.insert(attach);
		return attach;
	}

}
