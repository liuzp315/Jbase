/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.service;

import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Project: fw_admin <br/>
 * File: EntityService.java <br/>
 * Class: com.yxt.admin.service.sys.EntityService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月19日 下午2:50:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public abstract class BaseService {

	@Autowired
	public void setSpringBeetlSql(SpringBeetlSql springBeetlSql) {
		this.sqlManager = springBeetlSql.getSQLMananger();
	}

	protected SQLManager sqlManager = null;

	protected int updateById(Class<?> clazz, Map<String, Object> map) {
		StringBuilder sb = new StringBuilder("update ");
		sb.append(sqlManager.getNc().getTableName(clazz));
		sb.append(" set ");
		for (String key : map.keySet()) {
			if ("id".equals(key) || "sid".equals(key))
				continue;
			sb.append(key + "=#" + key + "#,");
		}
		sb.deleteCharAt(sb.lastIndexOf(",")).append(" where id = #id#");
		return sqlManager.executeUpdate(sb.toString(), map);
	}

	protected int updateBySid(Class<?> clazz, Map<String, Object> map) {
		StringBuilder sb = new StringBuilder("update ");
		sb.append(sqlManager.getNc().getTableName(clazz));
		sb.append(" set ");
		for (String key : map.keySet()) {
			if ("id".equals(key) || "sid".equals(key))
				continue;
			sb.append(key + "=#" + key + "#,");
		}
		sb.deleteCharAt(sb.lastIndexOf(",")).append(" where sid = #sid#");
		return sqlManager.executeUpdate(sb.toString(), map);
	}
	
}
