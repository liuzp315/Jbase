/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.entity;

import java.io.Serializable;

import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Project: fw_common <br/>
 * File: Areas.java <br/>
 * Class: com.yxt.common.entity.Areas <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2016年2月17日 下午3:11:10 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Table(name = "cms_article")
public class Areas extends TailBean implements Serializable {

	private static final long serialVersionUID = -402165124156845881L;
	
	private int areaId;
	private int parentId;
	private String areaName;
	private int isShow = 1;
	private int areaSort = 0;
	private String areaKey;
	private int areaType = 1;
	private int areaFlag = 1;

	/**
	 * @return the areaId
	 */
	public int getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the parentId
	 */
	public int getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * @param areaName
	 *            the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * @return the isShow
	 */
	public int getIsShow() {
		return isShow;
	}

	/**
	 * @param isShow
	 *            the isShow to set
	 */
	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

	/**
	 * @return the areaSort
	 */
	public int getAreaSort() {
		return areaSort;
	}

	/**
	 * @param areaSort
	 *            the areaSort to set
	 */
	public void setAreaSort(int areaSort) {
		this.areaSort = areaSort;
	}

	/**
	 * @return the areaKey
	 */
	public String getAreaKey() {
		return areaKey;
	}

	/**
	 * @param areaKey
	 *            the areaKey to set
	 */
	public void setAreaKey(String areaKey) {
		this.areaKey = areaKey;
	}

	/**
	 * @return the areaType
	 */
	public int getAreaType() {
		return areaType;
	}

	/**
	 * @param areaType
	 *            the areaType to set
	 */
	public void setAreaType(int areaType) {
		this.areaType = areaType;
	}

	/**
	 * @return the areaFlag
	 */
	public int getAreaFlag() {
		return areaFlag;
	}

	/**
	 * @param areaFlag
	 *            the areaFlag to set
	 */
	public void setAreaFlag(int areaFlag) {
		this.areaFlag = areaFlag;
	}

}
