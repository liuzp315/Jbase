/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.common.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.beetl.sql.core.TailBean;

/**
 * Project: fw_common <br/>
 * File: BaseEntity.java <br/>
 * Class: com.yxt.common.entity.BaseEntity <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 下午3:54:57 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
public class BaseEntity extends TailBean implements Serializable {

	private static final long serialVersionUID = -3902203005936127308L;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * @param sid
	 *            the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * @return the lockVersion
	 */
	public int getLockVersion() {
		return lockVersion;
	}

	/**
	 * @param lockVersion
	 *            the lockVersion to set
	 */
	public void setLockVersion(int lockVersion) {
		this.lockVersion = lockVersion;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the modifyTime
	 */
	public Date getModifyTime() {
		return modifyTime;
	}

	/**
	 * @param modifyTime
	 *            the modifyTime to set
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the isEnable
	 */
	public int getIsEnable() {
		return isEnable;
	}

	/**
	 * @param isEnable
	 *            the isEnable to set
	 */
	public void setIsEnable(int isEnable) {
		this.isEnable = isEnable;
	}

	/**
	 * @return the isDelete
	 */
	public int getIsDelete() {
		return isDelete;
	}

	/**
	 * @param isDelete
	 *            the isDelete to set
	 */
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	/**
	 * @return the createBySid
	 */
	public String getCreateBySid() {
		return createBySid;
	}

	/**
	 * @param createBySid
	 *            the createBySid to set
	 */
	public void setCreateBySid(String createBySid) {
		this.createBySid = createBySid;
	}

	/**
	 * @return the updateBySid
	 */
	public String getUpdateBySid() {
		return updateBySid;
	}

	/**
	 * @param updateBySid
	 *            the updateBySid to set
	 */
	public void setUpdateBySid(String updateBySid) {
		this.updateBySid = updateBySid;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	private int id;
	private String sid = UUID.randomUUID().toString();
	private int lockVersion = 0;
	private Date createTime = new Date();
	private Date modifyTime;
	private int state = 1;
	private int isEnable = 1;
	private int isDelete = 0;
	private String remarks;
	private String createBySid;
	private String updateBySid;

	@Override
	public String toString() {
		return "ClassName:" + getClass().getName() + ";id:" + getId() + ";createTime:" + getCreateTime();
	}

	@Override
	public int hashCode() {
		return super.hashCode() + getId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BaseEntity))
			return false;
		BaseEntity entity = (BaseEntity) obj;

		return this.getId() == entity.getId();
	}

}
