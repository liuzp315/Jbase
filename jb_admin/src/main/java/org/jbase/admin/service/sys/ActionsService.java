/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.jbase.admin.entity.sys.Actions;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Project: fw_admin <br/>
 * File: AdminService.java <br/>
 * Class: com.yxt.admin.service.system.AdminService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午9:44:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class ActionsService extends EntityService<Actions> {

	@Override
	protected Class<Actions> clzz() {
		return Actions.class;
	}

	/**
	 * 根据当前Servlet上下文获得所有Actions对象
	 */
	public List<Actions> listAllFromServletContext(HttpServletRequest request) {
		List<Actions> list = new ArrayList<Actions>();

		ServletContext servletContext = request.getSession().getServletContext();
		if (servletContext == null) {
			return null;
		}
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		// 请求url和处理方法的映射,获取所有的RequestMapping
		Map<String, HandlerMapping> allRequestMappings = BeanFactoryUtils.beansOfTypeIncludingAncestors(appContext,
				HandlerMapping.class, true, false);
		for (HandlerMapping handlerMapping : allRequestMappings.values()) {
			// RequestMappingHandlerMapping中的URL映射
			if (handlerMapping instanceof RequestMappingHandlerMapping) {
				RequestMappingHandlerMapping requestMappingHandlerMapping = (RequestMappingHandlerMapping) handlerMapping;
				Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping
						.getHandlerMethods();
				for (Map.Entry<RequestMappingInfo, HandlerMethod> requestMappingInfoHandlerMethodEntry : handlerMethods
						.entrySet()) {
					RequestMappingInfo requestMappingInfo = requestMappingInfoHandlerMethodEntry.getKey();
					HandlerMethod mappingInfoValue = requestMappingInfoHandlerMethodEntry.getValue();
					RequestMethodsRequestCondition methodCondition = requestMappingInfo.getMethodsCondition();
					String requestType = "";
					Set<RequestMethod> methods = methodCondition.getMethods();
					for (RequestMethod requestMethod : methods) {
						requestType = requestMethod.name();
						break;
					}
					String requestUrl = "";
					PatternsRequestCondition patternsCondition = requestMappingInfo.getPatternsCondition();
					Set<String> patterns = patternsCondition.getPatterns();
					for (String string : patterns) {
						requestUrl = string;
						break;
					}

					String controllerName = mappingInfoValue.getBeanType().toString();
					String requestMethodName = mappingInfoValue.getMethod().getName();
					Class<?>[] methodParamTypes = mappingInfoValue.getMethod().getParameterTypes();

					Actions actions = new Actions();
					actions.setUrl(requestUrl);
					actions.setControllerClassName(controllerName);
					actions.setRequestParameterTypes(methodParamTypes.toString());
					actions.setRequestMethod(requestMethodName);
					list.add(actions);
				}
			}
		}

		return list;
	}

}
