/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.service.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbase.admin.entity.sys.Role;
import org.jbase.common.service.EntityService;
import org.springframework.stereotype.Service;

/**
 * Project: fw_admin <br/>
 * File: AdminService.java <br/>
 * Class: com.yxt.admin.service.system.AdminService <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午9:44:38 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Service
public class RoleService extends EntityService<Role> {

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param name
	 * @return
	 */
	public Role getRoleByName(String name) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		List<Role> list = sqlManager.execute("select id from sys_role where name = #name#", Role.class, map);
		if (list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	public Role getOtherRoleByName(String name, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("id", id);
		List<Role> list = sqlManager.execute("select id from sys_role where name = #name# and id<>#id#", Role.class,
				map);
		if (list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param id
	 * @param state
	 */
	public void setState(String id, String state) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", state);
		map.put("id", id);
		sqlManager.executeUpdate("update sys_role set state=#state# where id = #id#", map);
	}

	@Override
	protected Class<Role> clzz() {
		return Role.class;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param id
	 * @return
	 */
	public List<Role> listByAdminId(int adminId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("adminId", adminId);
		List<Role> select = sqlManager.select("sys_role.listByAdminId", Role.class, map);
		return select;
	}

	/**
	 * 描述 : <描述函数实现的功能>. <br/>
	 * <p>
	 * 
	 * @param id
	 * @return
	 */
	public List<Role> listAllAndCheckedByAdminId(int adminId) {
		List<Role> checkedList = listByAdminId(adminId);
		List<Role> allList = list();
		for (Role role : allList) {
			if (checkedList.contains(role)) {
				role.set("isChecked", true);
			} else {
				role.set("isChecked", false);
			}
		}
		return allList;
	}

}
