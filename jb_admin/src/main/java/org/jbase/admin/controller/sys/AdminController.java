/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.admin.entity.sys.Admin;
import org.jbase.admin.entity.sys.Role;
import org.jbase.admin.service.sys.AdminService;
import org.jbase.admin.service.sys.RoleService;
import org.jbase.common.controller.EntityController;
import org.jbase.common.service.EntityService;
import org.jbase.common.utils.Requester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project: fw_admin <br/>
 * File: Admin.java <br/>
 * Class: com.yxt.admin.controller.system.Admin <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午3:08:15 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_sys_admin_controller")
@RequestMapping("/admin/sys/admin")
public class AdminController extends EntityController<Admin> {

	@Autowired
	private AdminService adminService;
	@Autowired
	private RoleService roleService;

	@RequestMapping("/userinfo")
	public void userinfo() {

	}

	@RequestMapping("/changepass")
	public void changepass() {

	}

	@Override
	public void add(HttpServletRequest request) {
		request.setAttribute("roleList", roleService.list());
		super.add(request);
	}

	@Override
	public String addSave(HttpServletRequest request) {
		Map<String, Object> params = Requester.warp(request).getParams();
		params.remove("roles");
		String[] roleIds = request.getParameterValues("roles");
		adminService.saveWithRoleIds(params, roleIds);
		return ok();
	}
	
	@Override
	public void edit(int id, HttpServletRequest request) {
		super.edit(id, request);
		List<Role> roleList = roleService.listAllAndCheckedByAdminId(id);
		request.setAttribute("roleList", roleList);
	}
	
	@Override
	public String editSave(HttpServletRequest request) {
		Map<String, Object> params = Requester.warp(request).getParams();
		params.remove("roles");
		String[] roleIds = request.getParameterValues("roles");
		adminService.updateWithRoleIds(params, roleIds);
		return ok();
	}

	@Override
	protected EntityService<Admin> service() {
		return adminService;
	}

}
