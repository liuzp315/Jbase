/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.jbase.common.servlet.ValidateCodeServlet;
import org.jbase.common.utils.Encodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Project: fw_admin <br/>
 * File: IndexController.java <br/>
 * Class: com.yxt.admin.controller.IndexController <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年11月30日 上午11:55:21 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_index_controller")
@RequestMapping("/admin")
public class IndexController {

	private static final Logger L = LoggerFactory.getLogger(IndexController.class);

	@RequestMapping("/index")
	public void index() {
		L.debug("index");
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public void login() {
		L.debug("login");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginDo(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		L.debug("loginDo");
		String loginName = request.getParameter("loginName");
		String loginPass = request.getParameter("password");
		String validateCode = request.getParameter("validateCode");
		String code = (String) request.getSession().getAttribute(ValidateCodeServlet.VALIDATE_CODE);
		if (StringUtils.isBlank(loginPass) || StringUtils.isBlank(loginName) || StringUtils.isBlank(validateCode)) {
			redirectAttributes.addFlashAttribute("error_message", "登录名、密码和验证码都必填！");
			return "redirect:/admin/login";
		}
		if (!validateCode.toUpperCase().equals(code)) {
			redirectAttributes.addFlashAttribute("error_message", "验证码错误！");
			return "redirect:/admin/login";
		}

		UsernamePasswordToken token = new UsernamePasswordToken(loginName, Encodes.md5(loginPass));
		String rememberMe = request.getParameter("rememberMe");
		if (StringUtils.isNotBlank(rememberMe) && "1".equals(rememberMe)) {
			token.setRememberMe(true);
		}
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			return "redirect:/admin/index";
		} catch (UnknownAccountException e) {
			redirectAttributes.addFlashAttribute("error_message", "用户名不存在！");
			return "redirect:/admin/login";
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error_message", "登录失败，请检查用户名或密码是否正确！");
			return "redirect:/admin/login";
		}
	}

	@RequestMapping("/logout")
	public String logout() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:login";
	}

	@RequestMapping("/error404")
	public void error404() {
		L.debug("error404");
	}

	@RequestMapping("/error403")
	public void error403() {
		L.debug("error403");
	}

}
