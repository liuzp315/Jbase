/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller.sys;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jbase.admin.entity.sys.Menu;
import org.jbase.admin.service.sys.MenuService;
import org.jbase.common.controller.EntityController;
import org.jbase.common.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Project: fw_admin <br/>
 * File: Admin.java <br/>
 * Class: com.yxt.admin.controller.system.Admin <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午3:08:15 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_sys_menu_controller")
@RequestMapping("/admin/sys/menu")
public class MenuController extends EntityController<Menu> {

	@Autowired
	private MenuService menuService;

	@Override
	protected EntityService<Menu> service() {
		return menuService;
	}

	@Override
	public void list(HttpServletRequest request) {
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = menuService.list();
		request.setAttribute("menuList", menuList);
	}
	
	@RequestMapping("/menuTree")
	public void menuTree(HttpServletRequest request) {
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = menuService.list();
		request.setAttribute("menuList", menuList);
	}
	
	@RequestMapping("/iconList")
	public void iconList(HttpServletRequest request) {
		List<Menu> menuList = new ArrayList<Menu>();
		menuList = menuService.list();
		request.setAttribute("menuList", menuList);
	}

}
