/*********************************************************
 *********************************************************
 ********************                  *******************
 *************                                ************
 *******                  _oo0oo_                  *******
 ***                     o8888888o                     ***
 *                       88" . "88                       *
 *                       (| -_- |)                       *
 *                       0\  =  /0                       *
 *                     ___/`---'\___                     *
 *                   .' \\|     |// '.                   *
 *                  / \\|||  :  |||// \                  *
 *                 / _||||| -:- |||||- \                 *
 *                |   | \\\  -  /// |   |                *
 *                | \_|  ''\---/''  |_/ |                *
 *                \  .-\__  '-'  ___/-. /                *
 *              ___'. .'  /--.--\  `. .'___              *
 *           ."" '<  `.___\_<|>_/___.' >' "".            *
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |          *
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /          *
 *      =====`-.____`.___ \_____/___.-`___.-'=====       *
 *                        `=---='                        *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      *
 *********__佛祖保佑__永无BUG__验收通过__钞票多多__*********
 *********************************************************/
package org.jbase.admin.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jbase.admin.entity.sys.Role;
import org.jbase.admin.service.sys.RoleService;
import org.jbase.common.utils.Pager;
import org.jbase.common.utils.Requester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

/**
 * Project: fw_admin <br/>
 * File: Admin.java <br/>
 * Class: com.yxt.admin.controller.system.Admin <br/>
 * Description: <描述类的功能>. <br/>
 * Copyright: Copyright (c) 2011 <br/>
 * Company: http://www.yxtsoft.com/ <br/>
 * Makedate: 2015年12月2日 上午3:08:15 <br/>
 * 
 * @author liuzhanhong
 * @version 1.0
 * @since 1.0
 */
@Controller("admin_sys_role_controller")
@RequestMapping("/admin/sys/role")
public class RoleController {

	private static final Logger L = LoggerFactory.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;

	@RequestMapping("/list")
	public void list(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = roleService.listWithPage(warp.getSearchKeys(),warp.getPager());
		request.setAttribute("pager", pager);
	}

	@ResponseBody
	@RequestMapping(value = "/listData", produces = "text/plain;charset=UTF-8")
	public String listData(HttpServletRequest request) {
		Requester warp = Requester.warp(request);
		Pager pager = roleService.listWithPage(warp.getSearchKeys(),warp.getPager());
		return JSON.toJSONString(pager);
	}

	@RequestMapping("/add")
	public void add() {
	}

	@ResponseBody
	@RequestMapping(value = "/addSave", produces = "text/plain;charset=UTF-8")
	public String addSave(HttpServletRequest request) {
		int key = roleService.insertHolderId(Requester.warp(request).getParams());
		L.debug("addSave: {}", key);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	@RequestMapping("/del")
	public String del(@RequestParam String ids) {
		roleService.deleteByIds(ids);
		return "redirect:list";
	}

	@RequestMapping("/edit")
	public void edit(@RequestParam int id, HttpServletRequest request) {
		Role role = roleService.fetchById(id);
		request.setAttribute("obj", role);
	}
	
	@ResponseBody
	@RequestMapping(value = "/editSave", produces = "text/plain;charset=UTF-8")
	public String editSave(HttpServletRequest request) {
		int key = roleService.update(Requester.warp(request).getParams());
		L.debug("addSave: {}", key);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retCode", "200");
		map.put("retMsg", "成功");
		map.put("retObj", "object");
		return JSON.toJSONString(map);
	}

	@RequestMapping("/setState")
	public String setState(@RequestParam String id, @RequestParam String state) {
		roleService.setState(id, state);
		return "redirect:list";
	}

	@ResponseBody
	@RequestMapping(value = "/checkQu", produces = "text/plain;charset=UTF-8")
	public String checkQu(@RequestParam String name, @RequestParam String param, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = request.getParameter("id");
		Role role = null;
		if (id == null) {
			role = roleService.getRoleByName(param);
		} else {
			role = roleService.getOtherRoleByName(param, id);
		}

		if (role == null) {
			map.put("status", "y");
			map.put("info", "名称可用");
		} else {
			map.put("info", "已存在同名称角色");
			map.put("status", "n");
		}
		return JSON.toJSONString(map);
	}

}
